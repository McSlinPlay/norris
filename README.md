# @mcslinplay/norris

This package exports a single unnamed function that returns a random Chuck Norris joke.

## Installation

```sh
npm install @mcslinplay/norris
```

## Usage

```js
const norris = require('@mcslinplay/norris');
const joke = norris();
console.log(joke);
```
