import fs from "fs";

/**
 *
 * @type {string[]}
 */
let objects = null

// const logger = loggers.get('discord')

function loadObjects() {
    objects = []
    fs.readFileSync('norris_jokes.txt')
        .toString()
        .split('\n')
        .map(s => s.trim())
        .filter(s => s !== "")
        .forEach((line) => {
            objects.push(line)
        })
}

export default function(): string {
    if (objects === null) {
        loadObjects()
    }
    return objects[Math.floor(Math.random() * objects.length)]
}
